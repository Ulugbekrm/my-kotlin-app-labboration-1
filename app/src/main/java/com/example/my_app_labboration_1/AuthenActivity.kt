package com.example.my_app_labboration_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class AuthenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authen)

        val userUsername: EditText = findViewById(R.id.et_usernameAuth)
        val userPassword: EditText = findViewById(R.id.et_passwordAuth)
        val btnReg: Button = findViewById(R.id.btn_loginAuth)
        val linkRegister: TextView = findViewById(R.id.link_register)

        linkRegister.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btnReg.setOnClickListener{
            val login = userUsername.text.toString().trim()
            val password = userPassword.text.toString().trim()

            if (login == "" || password == "")
                Toast.makeText(this, "Not all fields are filled in", Toast.LENGTH_LONG).show()
            else {
                val db = DB(this, null)
                val isAuthen = db.getUser(login, password)

                if (isAuthen) {
                    Toast.makeText(this, "User $login authenticate", Toast.LENGTH_LONG).show()
                    userUsername.text.clear()
                    userPassword.text.clear()

                    //Navigate from Login to Profil Page
                    val intent = Intent(this, ProfilActivity::class.java)
                    startActivity(intent)
                } else
                    Toast.makeText(this, "User $login not authenticate", Toast.LENGTH_LONG).show()
            }
        }
    }
}