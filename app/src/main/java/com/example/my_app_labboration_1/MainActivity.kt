package com.example.my_app_labboration_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val userUsername: EditText = findViewById(R.id.et_username)
        val userEmail: EditText = findViewById(R.id.et_email)
        val userPassword: EditText = findViewById(R.id.et_password)
        val btnReg: Button = findViewById(R.id.btn_login)
        val linkAuthentication: TextView = findViewById(R.id.link_authentication)

        linkAuthentication.setOnClickListener {
            val intent = Intent(this, AuthenActivity::class.java)
            startActivity(intent)
        }

        btnReg.setOnClickListener{
            val login = userUsername.text.toString().trim()
            val email = userEmail.text.toString().trim()
            val password = userPassword.text.toString().trim()

            if (login == "" || email == "" || password == "")
                Toast.makeText(this, "Not all fields are filled in", Toast.LENGTH_LONG).show()
            else {
                val user = User(login, email, password)

                val db = DB(this, null)
                db.addUser(user)
                Toast.makeText(this, "User $login add", Toast.LENGTH_LONG).show()

                userUsername.text.clear()
                userEmail.text.clear()
                userPassword.text.clear()
            }
        }
    }
}